<?php

/*
 * This file is part of Laravel Commentable.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Commentable;

use DraperStudio\ServiceProvider\ServiceProvider as BaseProvider;

class ServiceProvider extends BaseProvider
{
    protected $packageName = 'commentable';

    public function boot()
    {
        $this->setup(__DIR__)
             ->publishMigrations();
    }
}
